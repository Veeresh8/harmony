package veeresh.droid.harmony.service;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaBrowserServiceCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.devbrackets.android.exomedia.AudioPlayer;
import com.devbrackets.android.exomedia.listener.OnBufferUpdateListener;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.application.HarmonyApplication;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.models.CurrentPlayback.CurrentStation;

public class MusicService extends MediaBrowserServiceCompat implements OnBufferUpdateListener, MediaPlayer.OnCompletionListener, AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = MusicService.class.getSimpleName();

    private AudioPlayer mMediaPlayer;
    private MediaSessionCompat mMediaSessionCompat;
    private MediaControllerCompat.TransportControls transportControls;

    private CurrentStation currentStation;
    private ArrayList<CurrentStation> currentStationArrayList = new ArrayList<>();
    private int currentIndex, totalSize;
    private MediaNotificationManager mMediaNotificationManager;
    private long position;
    public Bitmap albumArtBitmap;

    private BroadcastReceiver mNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED);
                mMediaNotificationManager.createPauseNotification();
                mMediaPlayer.pause();
            }
        }
    };


    private MediaSessionCompat.Callback mMediaSessionCallback = new MediaSessionCompat.Callback() {

        @Override
        public void onPlay() {
            super.onPlay();
            if (!successfullyRetrievedAudioFocus()) {
                return;
            }
            mMediaPlayer.start();
            mMediaNotificationManager.createNotification();
            setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING);
        }

        @Override
        public void onPause() {
            super.onPause();
            setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED);
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
                mMediaNotificationManager.createPauseNotification();
            }
        }

        @Override
        public void onPlayFromSearch(String query, Bundle extras) {
            super.onPlayFromSearch(query, extras);
            skipToPosition(Integer.valueOf(query));
        }

        @Override
        public void onPlayFromMediaId(String streamURL, Bundle extras) {
            super.onPlayFromMediaId(streamURL, extras);

            RealmResults<CurrentStation> realmResults = RealmManager.stationsDetailsDAO().getCurrentQueue();

            currentStationArrayList.clear();

            currentStationArrayList.addAll(realmResults);

            currentStation = realmResults.get(extras.getInt("Index"));

            totalSize = realmResults.size();

            currentIndex = extras.getInt("Index");

            position = currentIndex;

            mMediaPlayer.reset();

            mMediaPlayer.setDataSource(Uri.parse(streamURL));

            mMediaPlayer.prepareAsync();

            mMediaPlayer.setOnPreparedListener(() -> {

                if (!successfullyRetrievedAudioFocus()) {
                    return;
                }

                mMediaPlayer.start();
                initMediaSessionMetadata();

            });

        }

        @Override
        public void onSkipToNext() {
            super.onSkipToNext();
            playNextTrack();
        }


        @Override
        public void onSkipToPrevious() {
            super.onSkipToPrevious();
            playPreviousTrack();
        }

        @Override
        public void onStop() {
            super.onStop();
            clearNotification();
        }

        @Override
        public void onAddQueueItem(MediaDescriptionCompat description) {
            super.onAddQueueItem(description);
        }

        @Override
        public void onAddQueueItem(MediaDescriptionCompat description, int index) {
            super.onAddQueueItem(description, index);
        }

        @Override
        public void onSeekTo(long pos) {
            super.onSeekTo(pos);
        }

    };

    private void clearNotification() {
        mMediaNotificationManager.stopNotification();
    }

    private void playNextTrack() {
        if (currentIndex < totalSize - 1) {
            currentStation = currentStationArrayList.get(++currentIndex);

            position = currentIndex;

            stopMedia();

            mMediaPlayer.reset();

            mMediaPlayer.setDataSource(Uri.parse(currentStation.getStreamURL()));

            mMediaPlayer.prepareAsync();

            mMediaPlayer.setOnPreparedListener(() -> {

                if (!successfullyRetrievedAudioFocus()) {
                    return;
                }

                mMediaPlayer.start();
                initMediaSessionMetadata();
            });
        }
    }


    private void skipToPosition(int trackPosition) {
        if (currentIndex < totalSize - 1) {

            currentIndex = trackPosition;

            currentStation = currentStationArrayList.get(trackPosition);

            position = currentIndex;

            stopMedia();

            mMediaPlayer.reset();

            mMediaPlayer.setDataSource(Uri.parse(currentStation.getStreamURL()));

            mMediaPlayer.prepareAsync();

            mMediaPlayer.setOnPreparedListener(() -> {

                if (!successfullyRetrievedAudioFocus()) {
                    return;
                }

                mMediaPlayer.start();
                initMediaSessionMetadata();
            });
        }
    }

    private void playPreviousTrack() {
        if (currentIndex == 0) {
            Log.d(TAG, "First in list");
        } else if (currentIndex <= totalSize - 1) {
            currentStation = currentStationArrayList.get(--currentIndex);

            position = currentIndex;

            stopMedia();

            mMediaPlayer.reset();

            mMediaPlayer.setDataSource(Uri.parse(currentStation.getStreamURL()));

            mMediaPlayer.prepareAsync();

            mMediaPlayer.setOnPreparedListener(() -> {

                if (!successfullyRetrievedAudioFocus()) {
                    return;
                }

                mMediaPlayer.start();
                initMediaSessionMetadata();
            });
        }
    }

    private void stopMedia() {
        if (mMediaPlayer == null) return;
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.stopPlayback();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RealmManager.open();
        initMediaPlayer();
        initMediaSession();
        initNoisyReceiver();
    }

    private void initNoisyReceiver() {
        IntentFilter filter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(mNoisyReceiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RealmManager.close();
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.abandonAudioFocus(this);
        unregisterReceiver(mNoisyReceiver);
        mMediaSessionCompat.release();
    }

    private void initMediaPlayer() {
        mMediaPlayer = new AudioPlayer(HarmonyApplication.getInstance());
        mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setVolume(1.0f, 1.0f);
    }


    private void initMediaSession() {
        ComponentName mediaButtonReceiver = new ComponentName(getApplicationContext(), MediaButtonReceiver.class);
        mMediaSessionCompat = new MediaSessionCompat(getApplicationContext(), "Tag", mediaButtonReceiver, null);

        mMediaSessionCompat.setCallback(mMediaSessionCallback);
        mMediaSessionCompat.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        transportControls = mMediaSessionCompat.getController().getTransportControls();

        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        mediaButtonIntent.setClass(this, MediaButtonReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0);

        mMediaSessionCompat.setMediaButtonReceiver(pendingIntent);

        setSessionToken(mMediaSessionCompat.getSessionToken());

        try {
            mMediaNotificationManager = new MediaNotificationManager(this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    private void setMediaPlaybackState(int state) {
        PlaybackStateCompat.Builder builder = new PlaybackStateCompat.Builder();
        if (state == PlaybackStateCompat.STATE_PLAYING) {
            builder.setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_PAUSE);
        } else {
            builder.setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_PLAY);
        }
        builder.setState(state, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 0);
        mMediaSessionCompat.setPlaybackState(builder.build());
    }

    private void initMediaSessionMetadata() {


        MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();

        Glide.with(this)
                .load(currentStation.getStationImage())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ART, resource);
                        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, currentStation.getStationName());
                        metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, position);
                        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, currentStation.getStationImage());
                        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI, currentStation.getStreamURL());
                        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, currentStation.getStationCountry());

                        albumArtBitmap = resource;

                        mMediaSessionCompat.setMetadata(metadataBuilder.build());

                    }
                });


        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI, currentStation.getStreamURL());
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, currentStation.getStationImage());
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, currentStation.getStationName());
        metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, position);
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, currentStation.getStationCountry());

        mMediaSessionCompat.setMetadata(metadataBuilder.build());
        mMediaSessionCompat.setActive(true);

        setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING);

        mMediaNotificationManager.startNotification();

        RealmManager.stationsDetailsDAO().setIsPlaying(currentStation.getStationId());

        RealmManager.stationsDetailsDAO().saveSectionItemById(currentStation.getStationId());

    }

    private boolean successfullyRetrievedAudioFocus() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        int result = audioManager.requestAudioFocus(this,
                AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        return result == AudioManager.AUDIOFOCUS_GAIN;
    }

    public boolean abandonFocus() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(this);
    }


    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid, @Nullable Bundle rootHints) {
        if (TextUtils.equals(clientPackageName, getPackageName())) {
            return new BrowserRoot(getString(R.string.app_name), null);
        }
        return null;
    }


    @Override
    public void onLoadChildren(@NonNull String parentId, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();
        result.sendResult(mediaItems);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS: {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.pause();
                    mMediaNotificationManager.createPauseNotification();
                }
                break;
            }
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT: {
                mMediaPlayer.pause();
                mMediaNotificationManager.createPauseNotification();
                abandonFocus();
                break;
            }
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK: {
                if (mMediaPlayer != null) {
                    mMediaPlayer.setVolume(0.25f, 0.25f);
                }
                break;
            }
            case AudioManager.AUDIOFOCUS_GAIN: {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.setVolume(1.0f, 1.0f);
                }

                break;
            }

        }
    }

    public NotificationCompat.Builder from(Context context, MediaSessionCompat mediaSession) {
        MediaControllerCompat controller = mediaSession.getController();
        MediaMetadataCompat mediaMetadata = controller.getMetadata();
        MediaDescriptionCompat description = mediaMetadata.getDescription();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder
                .setContentTitle(description.getTitle())
                .setContentText(description.getSubtitle())
                .setSubText(description.getDescription())
                .setLargeIcon(description.getIconBitmap())
                .setContentIntent(controller.getSessionActivity())
                .setDeleteIntent(MediaButtonReceiver.buildMediaButtonPendingIntent(context, PlaybackStateCompat.ACTION_STOP))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        return builder;
    }


    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MediaButtonReceiver.handleIntent(mMediaSessionCompat, intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onBufferingUpdate(@IntRange(from = 0L, to = 100L) int percent) {
        Log.d(TAG, "Buffering - " + percent);
    }


}
