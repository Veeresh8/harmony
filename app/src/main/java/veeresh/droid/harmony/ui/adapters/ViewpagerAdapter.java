package veeresh.droid.harmony.ui.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import veeresh.droid.harmony.ui.fragments.FavouriteStationFragment;
import veeresh.droid.harmony.ui.fragments.GenreFragment;
import veeresh.droid.harmony.ui.fragments.SelectedLanguagesFragment;


/**
 * Created by Veeresh8 on 9/8/2016.
 */
public class ViewpagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"STATIONS", "GENRES", "FAVORITES"};
    private Context context;

    public ViewpagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return SelectedLanguagesFragment.newInstance(position + 1);
        else if (position == 1)
            return GenreFragment.newInstance(position + 2);
        else
            return FavouriteStationFragment.newInstance(position + 3);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
