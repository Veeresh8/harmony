package veeresh.droid.harmony.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.models.CurrentPlayback.CurrentStation;


/**
 * Created by Veeresh8 on 9/11/2016.
 */
public class CarouselAdapter extends RealmRecyclerViewAdapter<CurrentStation, CarouselAdapter.Holder> {
    private Context context;
    public static final String TAG = CarouselAdapter.class.getSimpleName();

    public CarouselAdapter(@Nullable OrderedRealmCollection<CurrentStation> data, Context context) {
        super(data, true);
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.item_carousel, parent, false);
        return new Holder(contactView);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Glide.with(context).load(getData().get(position).getStationImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.white_centered_bordered_song_note_image)
                .placeholder(R.mipmap.ic_launcher).into(holder.stationImage);
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.station_image)
        ImageView stationImage;

        @OnClick(R.id.station_image)
        public void carouselOnClick() {
            EventBus.getDefault().post(new MyEvents.CarouselEvent(getAdapterPosition()));
        }


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
