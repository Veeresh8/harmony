package veeresh.droid.harmony.ui.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import veeresh.droid.harmony.R;


public class InfoActivity extends AppCompatActivity {

    @BindView(R.id.mail)
    TextView mail;
    @BindView(R.id.rating)
    TextView rating;
    @BindView(R.id.version)
    TextView version;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.designer)
    TextView designer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

        try {
            version.setText("Version - " + this.getPackageManager()
                    .getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @OnClick({R.id.mail, R.id.rating})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mail:
                openMail();
                break;
            case R.id.rating:
                launchPlayStore();
                break;
        }
    }

    private void openMail() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.setType("vnd.android.cursor.item/email");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"veeresh.charantimath8@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Harmony");
        startActivity(Intent.createChooser(emailIntent, "Send Email"));
    }

    private void launchPlayStore() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + this.getPackageName())));
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }
    }

    @OnClick(R.id.back)
    public void onViewClicked() {
        onBackPressed();
    }

    @OnClick(R.id.designer)
    public void openDesignerPage() {
        Uri uri = Uri.parse("https://www.linkedin.com/in/wellingtonicons/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);

    }
}
