package veeresh.droid.harmony.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.airbnb.lottie.LottieAnimationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.ui.adapters.FavoritesAdapter;


/**
 * Created by Veeresh8 on 9/8/2016.
 */
public class FavouriteStationFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private static final String TAG = FavouriteStationFragment.class.getSimpleName();

    private int mPage;

    @BindView(R.id.rv_favorites)
    RecyclerView recyclerView;
    @BindView(R.id.loading_layout)
    RelativeLayout loadingLayout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;


    private FavoritesAdapter adapter;

    public static FavouriteStationFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        FavouriteStationFragment fragment = new FavouriteStationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RealmManager.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite_station, container, false);
        ButterKnife.bind(this, view);
        RealmManager.open();
        initUI();
        return view;
    }


    private void setLoadingLayout() {
        if (adapter.getItemCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        } else {
            loadingLayout.setVisibility(View.VISIBLE);
            animationView.playAnimation();
            animationView.loop(true);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void checkUI(MyEvents.CheckFavEvent checkFavEvent) {
        if (checkFavEvent.isMustCheck())
            setLoadingLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void initUI() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new FavoritesAdapter(RealmManager.stationsDetailsDAO().loadFavStations(), getActivity());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
