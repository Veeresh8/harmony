package veeresh.droid.harmony.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;


/**
 * Created by Veeresh8 on 9/11/2016.
 */
public class GenreDetailsAdapter extends RealmRecyclerViewAdapter<Station, GenreDetailsAdapter.Holder> {

    private Context context;

    public GenreDetailsAdapter(@Nullable OrderedRealmCollection<Station> data, Context context) {
        super(data, true);
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.item_station_details, parent, false);
        return new Holder(contactView);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.language.setText(getData().get(position).getName());
        holder.countryName.setText(getData().get(position).getCountryName());
        Glide.with(context).load(getData().get(position).getImageUrl()).placeholder(R.drawable.ic_radio_orange_500_24dp).into(holder.stationImage);
        if (getData().get(position).isFavorite())
            holder.favCheck.setChecked(true);
        else
            holder.favCheck.setChecked(false);
    }

    private void animateCheck(CheckBox checkBox) {
        checkBox.setScaleX(0);
        checkBox.setScaleY(0);
        checkBox.animate().scaleX(1).scaleY(1).start();
    }


    @Override
    public int getItemCount() {
        return getData().size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.language)
        TextView language;
        @BindView(R.id.station_image)
        ImageView stationImage;
        @BindView(R.id.check_fav)
        CheckBox favCheck;
        @BindView(R.id.country)
        TextView countryName;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            favCheck.setOnClickListener(v -> {
                CheckBox checkBox = (CheckBox) v;
                if (checkBox.isChecked()) {
                    RealmManager.stationsDetailsDAO().saveToFavorites(getData().get(getAdapterPosition()));
                    animateCheck(checkBox);
                } else {
                    RealmManager.stationsDetailsDAO().removeFromFavorites(getData().get(getAdapterPosition()));
                }

            });
        }
    }
}
