package veeresh.droid.harmony.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;
import io.realm.RealmResults;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.models.SelectedLanguages.HeaderSectionModel;
import veeresh.droid.harmony.models.SelectedLanguages.LanguageSelected;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;
import veeresh.droid.harmony.ui.adapters.StationsLanguageAdapter;


/**
 * Created by Veeresh8 on 9/11/2016.
 */

public class SelectedLanguagesFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String SELECTED_LANGUAGE = "selected_language";
    public static final String SELECTED_GENRE = "selected_genre";
    @BindView(R.id.rv_stations)
    RecyclerView recyclerView;
    private int mPage;
    private StationsLanguageAdapter adapter;
    private ArrayList<HeaderSectionModel> headerSectionModelArrayList = new ArrayList<>();


    public static SelectedLanguagesFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        SelectedLanguagesFragment fragment = new SelectedLanguagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        RealmManager.open();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stations, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new StationsLanguageAdapter(RealmManager.stationsDetailsDAO().loadSectionData(), true, getActivity()) {
            @Override
            public void load() {
                Log.d("Load", "Reached End");
            }
        };
        recyclerView.setAdapter(adapter);
        recyclerView.post(() -> recyclerView.smoothScrollToPosition(headerSectionModelArrayList.size() - headerSectionModelArrayList.size()));
    }

    private void createStationData() {
        headerSectionModelArrayList.clear();
        RealmResults<LanguageSelected> results = RealmManager.languageSelectedDAO().loadLanguagesSelected();
        for (int i = 0; i < results.size(); i++) {
            HeaderSectionModel headerSectionModel = new HeaderSectionModel();
            headerSectionModel.setHeaderTitle(results.get(i).getLanguage());
            headerSectionModel.setType("Item");
            RealmList<Station> stationRealmList = new RealmList<>();
            RealmResults<Station> realmResults = RealmManager.stationsDetailsDAO().loadStations(results.get(i).getLanguage());
            stationRealmList.addAll(realmResults);
            headerSectionModel.setLanguageModelArrayList(stationRealmList);
            headerSectionModelArrayList.add(headerSectionModel);
        }
        RealmManager.stationsDetailsDAO().saveSectionData(headerSectionModelArrayList);
        recyclerView.post(() -> recyclerView.smoothScrollToPosition(headerSectionModelArrayList.size() - headerSectionModelArrayList.size()));
    }


    @Subscribe
    public void dataLoaded(MyEvents.DataLoadedEvent event) {
        if (event.isDataLoaded()) {
            createStationData();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onDestroy() {
        RealmManager.close();
        super.onDestroy();
    }
}
