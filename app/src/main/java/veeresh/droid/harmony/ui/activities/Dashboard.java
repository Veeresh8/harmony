package veeresh.droid.harmony.ui.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.cleveroad.play_widget.PlayLayout;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import net.orange_box.storebox.StoreBox;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.models.AllLanguagesResponse.AllLanguagesResponse;
import veeresh.droid.harmony.models.ClientDetailsInfo.ClientDetails;
import veeresh.droid.harmony.models.SelectedLanguages.HeaderSectionModel;
import veeresh.droid.harmony.models.SelectedLanguages.LanguageSelected;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;
import veeresh.droid.harmony.models.StationDetailsResponse.StationDetails;
import veeresh.droid.harmony.network.RetrofitConnection;
import veeresh.droid.harmony.service.MusicService;
import veeresh.droid.harmony.ui.adapters.CarouselAdapter;
import veeresh.droid.harmony.ui.adapters.ViewpagerAdapter;
import veeresh.droid.harmony.ui.fragments.StationsDetailFragment;
import veeresh.droid.harmony.utilities.FontCache;
import veeresh.droid.harmony.utilities.GetPhoneInfo;
import veeresh.droid.harmony.utilities.Preferences;
import veeresh.droid.harmony.utilities.SharedPreferences;
import veeresh.droid.harmony.utilities.SharedPrefsKeys;

import static veeresh.droid.harmony.ui.fragments.SelectedLanguagesFragment.SELECTED_LANGUAGE;

public class Dashboard extends AppCompatActivity {

    private static final String TAG = Dashboard.class.getSimpleName();
    public static final String TYPE = "type";

    @BindView(R.id.play_pause_checkbox)
    CheckBox playPauseCheckboxMini;
    @BindView(R.id.station_image_preview)
    ImageView stationImagePreview;
    @BindView(R.id.station_name)
    TextView stationName;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.root_layout)
    RelativeLayout coordinatorLayout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.loading_layout)
    RelativeLayout loadingLayout;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingPaneLayout;
    @BindView(R.id.playLayout)
    PlayLayout musicView;
    @BindView(R.id.discrete_scroll)
    DiscreteScrollView discreteScrollView;
    @BindView(R.id.animation_helper_text)
    TextView animationHelperText;
    @BindView(R.id.view_divide)
    View divider;
    @BindView(R.id.sub_player)
    RelativeLayout musicPlayerHeader;
    @BindView(R.id.music_player_header)
    RelativeLayout playerHeader;
    @BindView(R.id.progress)
    ProgressBar progressBar;


    private String streamURL, imageURL;
    private int mCurrentState;
    private Bundle savedInstanceState;
    private RealmList<LanguageSelected> realmLanguagesList = new RealmList<>();
    private StringBuilder stringBuilder = new StringBuilder();
    private String token = null;
    private MediaBrowserCompat mMediaBrowserCompat;
    private MediaControllerCompat mMediaControllerCompat;
    private Preferences preferences;
    private ArrayList<String> selectedLanguages = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private MediaBrowserCompat.ConnectionCallback mMediaBrowserCompatConnectionCallback = new MediaBrowserCompat.ConnectionCallback() {

        @Override
        public void onConnected() {
            super.onConnected();
            try {
                mMediaControllerCompat = new MediaControllerCompat(Dashboard.this, mMediaBrowserCompat.getSessionToken());
                mMediaControllerCompat.registerCallback(mMediaControllerCompatCallback);
                setSupportMediaController(mMediaControllerCompat);
                Log.d(TAG, "Connected");
            } catch (RemoteException e) {
                Log.d(TAG, e.getMessage());
            }
        }
    };


    private void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }


    @OnClick(R.id.info)
    public void onViewClicked() {
        startActivity(new Intent(Dashboard.this, InfoActivity.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        sharedPreferences = new SharedPreferences(this);
        RealmManager.open();
        this.savedInstanceState = savedInstanceState;
        preferences = StoreBox.create(this, Preferences.class);
        setStatusBarColor();
        setUpViewPager();


        if (preferences.getToken() == null) {
            textAnimation();
            setData();
        } else {
            divider.setVisibility(View.VISIBLE);
            musicPlayerHeader.setVisibility(View.VISIBLE);
        }


        mMediaBrowserCompat = new MediaBrowserCompat(this, new ComponentName(this, MusicService.class),
                mMediaBrowserCompatConnectionCallback, getIntent().getExtras());

        mMediaBrowserCompat.connect();

        initSlidingLayer();

        playPauseCheckboxMini.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                getSupportMediaController().getTransportControls().play();
            else
                getSupportMediaController().getTransportControls().pause();

        });

        slidingPaneLayout.setTouchEnabled(false);

        stationName.setText("No Stations Playing");

        musicView.setProgressEnabled(false);

        musicView.setOnButtonsClickListener(new PlayLayout.OnButtonsClickListener() {
            @Override
            public void onShuffleClicked() {

            }

            @Override
            public void onSkipPreviousClicked() {
                getSupportMediaController().getTransportControls().skipToPrevious();

            }

            @Override
            public void onSkipNextClicked() {
                getSupportMediaController().getTransportControls().skipToNext();

            }

            @Override
            public void onRepeatClicked() {

            }

            @Override
            public void onPlayButtonClicked() {
                playButtonClicked();
            }
        });


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2)
                    EventBus.getDefault().post(new MyEvents.CheckFavEvent(true));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void textAnimation() {
        animationHelperText.setVisibility(View.VISIBLE);
        final int[] array = {R.string.loading_1, R.string.loading_2, R.string.loading_3};
        animationHelperText.post(new Runnable() {
            int i = 0;

            @Override
            public void run() {
                animationHelperText.setText(array[i]);
                i++;
                if (i == 3)
                    i = 0;
                animationHelperText.postDelayed(this, 2000);
            }
        });
    }

    private void getStationData() {
        selectedLanguages.add("English");
        selectedLanguages.add("Hindi");
        selectedLanguages.add("Arabic");
        selectedLanguages.add("Punjabi");
        selectedLanguages.add("Tamil");
        selectedLanguages.add("Telugu");
        selectedLanguages.add("Bangla");
        selectedLanguages.add("Malayalam");

        realmLanguagesList.clear();

        for (int i = 0; i < selectedLanguages.size(); i++) {
            LanguageSelected realmString = new LanguageSelected();
            realmString.setLanguage(selectedLanguages.get(i));
            realmLanguagesList.add(realmString);
        }

        RealmManager.languageSelectedDAO().saveLanguageSelected(realmLanguagesList);

        getLanguages(selectedLanguages);

    }


    private void playButtonClicked() {
        if (musicView == null) {
            return;
        }
        if (musicView.isOpen()) {
            getSupportMediaController().getTransportControls().pause();
            musicView.startDismissAnimation();
        } else {
            getSupportMediaController().getTransportControls().play();
            musicView.startRevealAnimation();
        }
    }


    @Subscribe
    public void playStation(MyEvents.StationPlaybackEvent playbackEvent) {
        Log.d(TAG, playbackEvent.getStation().toString());

        EventBus.getDefault().post(new MyEvents.IsInProgressEvent(true));

        RealmManager.stationsDetailsDAO().saveCurrentlyPlayingQueue(playbackEvent.getOrderedRealmCollection());

        Bundle bundle = new Bundle();
        bundle.putString("StationName", playbackEvent.getStation().getName());
        bundle.putString("Country", playbackEvent.getStation().getCountryName());
        bundle.putString("StationImage", playbackEvent.getStation().getImageUrl());
        bundle.putInt("Index", playbackEvent.getIndex());

        boolean exists = RealmManager.stationsDetailsDAO().checkIfRecentExists("Recently Played");
        if (!exists) {
            HeaderSectionModel headerSectionModel = new HeaderSectionModel();
            headerSectionModel.setHeaderTitle("Recently Played");
            headerSectionModel.setType("Header");
            RealmList<Station> stationRealmList = new RealmList<>();
            stationRealmList.add(playbackEvent.getStation());
            headerSectionModel.setLanguageModelArrayList(stationRealmList);
            ArrayList<HeaderSectionModel> headerSectionModelArrayList = new ArrayList<>();
            headerSectionModelArrayList.add(headerSectionModel);
            RealmManager.stationsDetailsDAO().saveSectionData(headerSectionModelArrayList);
        } else {
            RealmManager.stationsDetailsDAO().saveSectionItem(playbackEvent);
        }

        getSupportMediaController().getTransportControls()
                .playFromMediaId(playbackEvent.getStation().getStreamUrl(), bundle);
    }

    private void showLottieAnimation(boolean mustShow) {
        if (mustShow) {
            musicPlayerHeader.setVisibility(View.GONE);
            playerHeader.setBackgroundColor(Color.parseColor("#FFFFFF"));
            divider.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
            animationView.playAnimation();
            animationView.loop(true);
        } else {
            divider.setVisibility(View.VISIBLE);
            musicPlayerHeader.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
            animationView.cancelAnimation();
            animationHelperText.setVisibility(View.GONE);
        }
    }

    public void initSlidingLayer() {
        discreteScrollView.setAdapter(new CarouselAdapter(RealmManager.stationsDetailsDAO().getCurrentQueue(), this));
        discreteScrollView.setItemTransitionTimeMillis(300);
        discreteScrollView.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());
    }

    @Subscribe
    public void onCarouselEvent(MyEvents.CarouselEvent carouselEvent) {
        String position = String.valueOf(carouselEvent.getPosition());
        getSupportMediaController().getTransportControls().playFromSearch(position, null);
    }

    private void getLanguages(ArrayList<String> selectedLanguages) {
        for (String station : selectedLanguages) {
            stringBuilder.append(station);
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        getStationData(stringBuilder);
    }

    private void getStationData(StringBuilder stringBuilder) {
        RetrofitConnection.getService().getStations(stringBuilder).enqueue(new Callback<StationDetails>() {
            @Override
            public void onResponse(Call<StationDetails> call, Response<StationDetails> response) {
                if (response.isSuccessful()) {
                    runOnUiThread(() -> saveStations(response));
                } else {
                    showLottieAnimation(false);
                    retry();

                }
            }

            @Override
            public void onFailure(Call<StationDetails> call, Throwable t) {
                showLottieAnimation(false);
                retry();
            }
        });
    }

    private void saveStations(Response<StationDetails> response) {
        RealmManager.stationsDetailsDAO().saveStations(response.body().getStation());
        showLottieAnimation(false);
        sharedPreferences.putBoolean(SharedPrefsKeys.HAS_STATIONS, true);
        EventBus.getDefault().post(new MyEvents.DataLoadedEvent(true));
    }

    private void setUpViewPager() {
        viewPager.setAdapter(new ViewpagerAdapter(getSupportFragmentManager(),
                Dashboard.this));
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        changeTabsFont();

    }


    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(FontCache.bold);
                }
            }
        }
    }

    @Subscribe
    public void getLanguageSelected(MyEvents.SelectedLanguageEvent selectedLanguageEvent) {
        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }

            String type = selectedLanguageEvent.getType();

            StationsDetailFragment stationsDetailFragment = new StationsDetailFragment();
            Bundle args = new Bundle();
            args.putString(SELECTED_LANGUAGE, selectedLanguageEvent.getSelectedLanguage());
            args.putString(TYPE, type);
            stationsDetailFragment.setArguments(args);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, stationsDetailFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        RealmManager.close();
        super.onDestroy();
        if (getSupportMediaController() != null && getSupportMediaController().getPlaybackState() != null) {
            getSupportMediaController().getTransportControls().pause();
            getSupportMediaController().getTransportControls().stop();
        }

        mMediaBrowserCompat.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private MediaControllerCompat.Callback mMediaControllerCompatCallback = new MediaControllerCompat.Callback() {

        @Override
        public void onPlaybackStateChanged(PlaybackStateCompat state) {
            super.onPlaybackStateChanged(state);

            if (state == null) {
                return;
            }

            switch (state.getState()) {
                case PlaybackStateCompat.STATE_PLAYING: {
                    slidingPaneLayout.setVisibility(View.VISIBLE);
                    slidingPaneLayout.setTouchEnabled(true);
                    stationImagePreview.setVisibility(View.VISIBLE);
                    playPauseCheckboxMini.setVisibility(View.VISIBLE);
                    playPauseCheckboxMini.setChecked(true);
                    musicView.startRevealAnimation();
                    showProgress(false);
                    discreteScrollView.smoothScrollToPosition((int) getSupportMediaController().getMetadata().getLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER));
                    setImageForItem(getSupportMediaController().getMetadata().getString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI));
                    streamURL = getSupportMediaController().getMetadata().getString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI);
                    stationName.setText(getSupportMediaController().getMetadata().getString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE));
                    break;
                }
                case PlaybackStateCompat.STATE_PAUSED: {
                    playPauseCheckboxMini.setChecked(false);
                    musicView.startDismissAnimation();
                    break;
                }
            }
        }
    };


    @Override
    public void onBackPressed() {
        if (slidingPaneLayout != null && (slidingPaneLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || slidingPaneLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }


    @Subscribe
    public void inProgress(MyEvents.IsInProgressEvent event) {
        showProgress(true);
    }

    private void showProgress(boolean mustShow) {
        if (mustShow) {
            progressBar.setVisibility(View.VISIBLE);
            stationName.setText("Streaming");
            stationImagePreview.setVisibility(View.INVISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            stationImagePreview.setVisibility(View.VISIBLE);
        }
    }


    private void setImageForItem(String imageURL) {

        if (!this.isFinishing()) {
            Glide.with(this)
                    .load(imageURL)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .error(R.drawable.white_centered_bordered_song_note_image)
                    .into(stationImagePreview);

            Glide.with(this)
                    .load(imageURL)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .error(R.drawable.white_centered_bordered_song_note_image)
                    .into(imageTarget);
        }

    }


    private SimpleTarget<GlideDrawable> imageTarget = new SimpleTarget<GlideDrawable>() {
        @Override
        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
            musicView.setImageDrawable(resource);
        }

        @Override
        public void onLoadFailed(Exception e, Drawable errorDrawable) {
            super.onLoadFailed(e, errorDrawable);
            musicView.setImageDrawable(errorDrawable);
        }
    };


    private void setData() {

        showLottieAnimation(true);

        PackageInfo packageInfo = null;

        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setDevice_model(GetPhoneInfo.getDeviceName());
        clientDetails.setApp_version(packageInfo.versionCode);
        clientDetails.setOs("Android");
        clientDetails.setOs_version(GetPhoneInfo.androidVersion);
        clientDetails.setNotification_token(FirebaseInstanceId.getInstance().getToken());


        RetrofitConnection.getService().getLanguages(clientDetails).enqueue(new Callback<AllLanguagesResponse>() {
            @Override
            public void onResponse(Call<AllLanguagesResponse> call, final Response<AllLanguagesResponse> response) {
                if (response.isSuccessful()) {
                    runOnUiThread(() -> {
                        token = response.body().getToken();
                        preferences.setToken(token);
                        getStationData();

                    });
                } else {
                    showLottieAnimation(false);
                    retry();
                }
            }

            @Override
            public void onFailure(Call<AllLanguagesResponse> call, Throwable t) {
                showLottieAnimation(false);
                retry();
            }
        });
    }


    @OnClick(R.id.refresh)
    public void refreshData() {
        if (!this.isFinishing()) {
            new BottomDialog.Builder(Dashboard.this)
                    .setTitle("Sync with server for new stations")
                    .setPositiveText("Yes")
                    .setCancelable(true)
                    .setPositiveBackgroundColorResource(R.color.colorAccent)
                    .setPositiveTextColorResource(android.R.color.black)
                    .onPositive(bottomDialog -> {
                        textAnimation();
                        setData();
                    }).show();
        }


    }


    private void retry() {
        if (!this.isFinishing()) {
            new BottomDialog.Builder(Dashboard.this)
                    .setTitle("Our Servers Are Busy!")
                    .setContent("Try Again")
                    .setPositiveText("Yes")
                    .setPositiveBackgroundColorResource(R.color.colorAccent)
                    .setPositiveTextColorResource(android.R.color.black)
                    .onPositive(bottomDialog -> {
                        textAnimation();
                        setData();
                    }).show();

        }
    }
}
