package veeresh.droid.harmony.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.models.SelectedLanguages.HeaderSectionModel;


/**
 * Created by Veeresh8 on 9/11/2016.
 */

public abstract class StationsLanguageAdapter extends RealmRecyclerViewAdapter<HeaderSectionModel, RecyclerView.ViewHolder> {

    private Context context;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;


    public StationsLanguageAdapter(@Nullable OrderedRealmCollection<HeaderSectionModel> data, boolean autoUpdate, Context context) {
        super(data, true);
        this.context = context;
        RealmManager.open();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_station_languges_header, parent, false);
            return new VHHeader(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_station_languges, parent, false);
            return new VHItem(v);
        }
        throw new RuntimeException("View type not found " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if ((position >= getItemCount() - 1)) {
            load();
        }

        if (holder instanceof VHHeader) {
            VHHeader header = (VHHeader) holder;
            header.language.setText("Recently Played");
            HeaderSectionModel realmResults = RealmManager.stationsDetailsDAO().loadHeaderData();
            if (realmResults != null && realmResults.getLanguageModelArrayList().size() > 0) {
                header.recyclerView.setVisibility(View.VISIBLE);
                header.btnMore.setVisibility(View.VISIBLE);
                header.language.setVisibility(View.VISIBLE);
                HeaderSectionModel headerSectionModel = RealmManager.stationsDetailsDAO().loadHeaderData();
                SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(realmResults.getLanguageModelArrayList(), true, context, headerSectionModel.getLanguageModelArrayList());
                header.recyclerView.setAdapter(itemListDataAdapter);
            } else {
                header.recyclerView.setVisibility(View.GONE);
                header.btnMore.setVisibility(View.GONE);
                header.language.setVisibility(View.GONE);
            }
        } else if (holder instanceof VHItem) {
            VHItem item = (VHItem) holder;
            item.language.setText(getData().get(position - 1).getHeaderTitle());
            SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(getData().get(position - 1).getLanguageModelArrayList(), true, context, getData().get(position - 1).getLanguageModelArrayList());
            item.recyclerView.setAdapter(itemListDataAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return getData().size() + 1;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;
        else
            return TYPE_ITEM;
    }


    public class VHHeader extends RecyclerView.ViewHolder {
        @BindView(R.id.language)
        TextView language;
        @BindView(R.id.rv_items)
        RecyclerView recyclerView;
        @BindView(R.id.tv_more)
        TextView btnMore;

        public VHHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
            snapHelper.attachToRecyclerView(recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);

            btnMore.setOnClickListener(v -> EventBus.getDefault().post(new MyEvents.SelectedLanguageEvent("Recently Played", "Recent")));

        }
    }

    public class VHItem extends RecyclerView.ViewHolder {
        @BindView(R.id.language)
        TextView language;
        @BindView(R.id.rv_items)
        RecyclerView recyclerView;
        @BindView(R.id.tv_more)
        TextView btnMore;

        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
            snapHelper.attachToRecyclerView(recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);

            btnMore.setOnClickListener(v -> EventBus.getDefault().post(new MyEvents.SelectedLanguageEvent(getData().get(getAdapterPosition() - 1).getHeaderTitle(), "Language")));

        }
    }

    public abstract void load();

}
