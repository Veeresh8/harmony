package veeresh.droid.harmony.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.RealmResults;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.models.SelectedLanguages.HeaderSectionModel;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;
import veeresh.droid.harmony.ui.activities.Dashboard;
import veeresh.droid.harmony.ui.adapters.StationDetailsAdapter;

public class StationsDetailFragment extends Fragment {

    private static final String TAG = StationsDetailFragment.class.getSimpleName();

    @BindView(R.id.rv_station_list)
    RecyclerView recyclerView;
    @BindView(R.id.station_name)
    TextView stationName;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;
    @BindView(R.id.action_sort)
    ImageView actionSort;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.action_search)
    ImageView actionSearch;
    Unbinder unbinder;
    @BindView(R.id.close)
    ImageView close;

    private RealmResults<Station> stationDetailsArrayList;
    private StationDetailsAdapter adapter;
    private String selectedLanguage, type;
    private int selectedPosition = -1;
    private HeaderSectionModel realmList;

    private void initUI() {
        switch (type) {
            case "Language":
                adapter = new StationDetailsAdapter(RealmManager.stationsDetailsDAO().loadStations(selectedLanguage), getActivity());
                break;
            case "Recent":
                realmList = RealmManager.stationsDetailsDAO().loadHeaderData();
                adapter = new StationDetailsAdapter(realmList.getLanguageModelArrayList(), getActivity());
                break;
            case "Genre":
                adapter = new StationDetailsAdapter(RealmManager.stationsDetailsDAO().loadGenre(selectedLanguage), getActivity());
                break;
        }

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void search() {
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.custom_cursor);
        searchView.setEllipsize(true);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0) {
                    if (type.equals("Language")) {
                        adapter.updateData(RealmManager.stationsDetailsDAO().loadStations(selectedLanguage));
                    } else if (type.equals("Recent")) {
                        adapter = new StationDetailsAdapter(realmList.getLanguageModelArrayList(), getActivity());
                    } else {
                        adapter.updateData(RealmManager.stationsDetailsDAO().loadGenre(selectedLanguage));
                    }
                } else {
                    adapter.updateData(RealmManager.stationsDetailsDAO().loadSearch(newText.toLowerCase(), selectedLanguage, type));
                }
                return false;
            }
        });
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter)
            return MoveAnimation.create(MoveAnimation.RIGHT, enter, 500);
        else
            return MoveAnimation.create(MoveAnimation.LEFT, enter, 500);

    }

    private void validateRecyclerView() {
        if (adapter.getData() != null && adapter.getData().size() == 0) {
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RealmManager.close();
    }

    @OnClick(R.id.action_sort)
    public void onViewClicked() {

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Most Played");
        arrayList.add("Most Liked");
        arrayList.add("A-Z");
        arrayList.add("Z-A");

        new MaterialDialog.Builder(getActivity())
                .title("Sort By")
                .items(arrayList)
                .itemsCallbackSingleChoice(selectedPosition, (dialog, view, which, text) -> {
                    if (which == 0) {
                        sortByMostPlayed();
                        selectedPosition = which;
                    } else if (which == 1) {
                        sortByMostLiked();
                        selectedPosition = which;
                    } else if (which == 2) {
                        sortAscending();
                        selectedPosition = which;
                    } else if (which == 3) {
                        sortDescending();
                        selectedPosition = which;
                    }
                    return true;
                })
                .positiveText("Done")
                .show();
    }

    private void sortDescending() {
        adapter.updateData(RealmManager.stationsDetailsDAO().loadNameDescending(selectedLanguage, type));
    }

    private void sortAscending() {
        adapter.updateData(RealmManager.stationsDetailsDAO().loadNameAscending(selectedLanguage, type));
    }

    private void sortByMostLiked() {
        adapter.updateData(RealmManager.stationsDetailsDAO().loadFavorites(selectedLanguage, type));
    }

    private void sortByMostPlayed() {
        adapter.updateData(RealmManager.stationsDetailsDAO().loadMostPlayed(selectedLanguage, type));
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stations_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        RealmManager.open();
        selectedLanguage = getArguments().getString(SelectedLanguagesFragment.SELECTED_LANGUAGE);
        type = getArguments().getString(Dashboard.TYPE);
        if (type != null && !type.equals("Recent")) {
            actionSearch.setVisibility(View.VISIBLE);
            actionSort.setVisibility(View.VISIBLE);
        }
        stationName.setText(selectedLanguage);
        initUI();
        validateRecyclerView();
        search();
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.action_search)
    public void onSearchViewClicked() {
        searchView.showSearch();
    }

    @OnClick(R.id.close)
    public void onCloseViewClicked() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }
}
