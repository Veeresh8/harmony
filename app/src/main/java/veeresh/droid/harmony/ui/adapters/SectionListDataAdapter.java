package veeresh.droid.harmony.ui.adapters;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;

public class SectionListDataAdapter extends RealmRecyclerViewAdapter<Station, SectionListDataAdapter.SingleItemRowHolder> {

    private Context context;
    private OrderedRealmCollection<Station> collection;

    public SectionListDataAdapter(@Nullable OrderedRealmCollection<Station> data, boolean autoUpdate, Context context, OrderedRealmCollection<Station> collection) {
        super(data, true);
        this.context = context;
        this.collection = collection;
    }


    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        return new SingleItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {
        holder.stationName.setText(getData().get(i).getName());
        Glide.with(context).load(getData().get(i).getImageUrl()).placeholder(R.drawable.ic_radio_orange_500_24dp).into(holder.stationImage);

    }

    @Override
    public int getItemCount() {
        return (null != getData() ? getData().size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        private TextView stationName;

        private ImageView stationImage;

        private SingleItemRowHolder(View view) {
            super(view);

            this.stationName = (TextView) view.findViewById(R.id.station_name);
            this.stationImage = (ImageView) view.findViewById(R.id.station_image);


            stationImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new MyEvents.StationPlaybackEvent(getData().get(getAdapterPosition()), collection, getAdapterPosition()));
                }
            });


        }

    }

}