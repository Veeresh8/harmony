package veeresh.droid.harmony.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import veeresh.droid.harmony.R;

/**
 * Created by Veeresh on 4/29/17.
 */

public class LanguagePreferenceFragment extends DialogFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_language_preference, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<String> languages = new ArrayList<>();
        languages.add("English");
        languages.add("Hindi");
        languages.add("Tamil");
        languages.add("Arabic");
        languages.add("Punjabi");
        languages.add("Bangla");
        languages.add("Telugu");
        languages.add("Malay");
        languages.add("Malayalam");

        new MaterialDialog.Builder(getActivity())
                .title("Choose Languages")
                .items(languages)
                .itemsCallbackMultiChoice(null, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        /**
                         * If you use alwaysCallMultiChoiceCallback(), which is discussed below,
                         * returning false here won't allow the newly selected check box to actually be selected
                         * (or the newly unselected check box to be unchecked).
                         * See the limited multi choice dialog example in the sample project for details.
                         **/
                        return true;
                    }
                })
                .positiveText("Done")
                .show();

    }


}
