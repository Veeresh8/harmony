package veeresh.droid.harmony.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.events.MyEvents;


/**
 * Created by Veeresh8 on 9/11/2016.
 */
public class GenreFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    public static final String BOLLYWOOD = "Bollywood";
    public static final String HITS = "Hits";
    public static final String ROCK = "Rock";
    public static final String POP = "Pop";
    public static final String FUNK = "Funk";
    public static final String TALK_NEWS = "Talk-News";


    @BindView(R.id.bollywood)
    CardView bollywood;
    @BindView(R.id.pop)
    CardView pop;
    @BindView(R.id.funk)
    CardView funk;
    @BindView(R.id.hits)
    CardView hits;
    @BindView(R.id.talk_news)
    CardView talkNews;
    @BindView(R.id.rock)
    CardView oldies;
    Unbinder unbinder;
    private int mPage;

    public static GenreFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        GenreFragment fragment = new GenreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_genre, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.bollywood, R.id.pop, R.id.funk, R.id.hits, R.id.talk_news, R.id.rock})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bollywood:
                sendEvent(BOLLYWOOD);
                break;
            case R.id.pop:
                sendEvent(POP);
                break;
            case R.id.funk:
                sendEvent(FUNK);
                break;
            case R.id.hits:
                sendEvent(HITS);
                break;
            case R.id.talk_news:
                sendEvent(TALK_NEWS);
                break;
            case R.id.rock:
                sendEvent(ROCK);
                break;
        }
    }

    private void sendEvent(String genre) {
        EventBus.getDefault().post(new MyEvents.SelectedLanguageEvent(genre, "Genre"));
    }
}
