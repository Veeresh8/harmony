package veeresh.droid.harmony.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import veeresh.droid.harmony.R;
import veeresh.droid.harmony.database.RealmManager;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;


/**
 * Created by Veeresh on 3/11/17.
 */


public class FavoritesAdapter extends RealmRecyclerViewAdapter<Station, FavoritesAdapter.ViewHolder> {

    private Context context;

    public FavoritesAdapter(OrderedRealmCollection<Station> orderedRealmCollection, Context context) {
        super(orderedRealmCollection, true);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_station_fav, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.stationName.setText(getData().get(position).getName());
        holder.countryName.setText(getData().get(position).getCountryName());
        Glide.with(context).load(getData().get(position).getImageUrl()).placeholder(R.drawable.ic_radio_orange_500_24dp).into(holder.stationImage);

        if (getData().get(position).isFavorite())
            holder.favCheck.setChecked(true);
        else
            holder.favCheck.setChecked(false);

        EventBus.getDefault().post(new MyEvents.ValidateRecyclerViewEvent(true));

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.language)
        TextView stationName;
        @BindView(R.id.check_fav)
        CheckBox favCheck;
        @BindView(R.id.station_image)
        ImageView stationImage;
        @BindView(R.id.country)
        TextView countryName;


        @OnClick(R.id.relative_layout)
        public void onClick() {
            EventBus.getDefault().post(new MyEvents.StationPlaybackEvent(getData().get(getAdapterPosition()), getData(), getAdapterPosition()));
        }


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            favCheck.setOnClickListener(v -> {
                CheckBox checkBox = (CheckBox) v;
                if (!checkBox.isChecked()) {
                    RealmManager.stationsDetailsDAO().removeFromFavorites(getData().get(getAdapterPosition()));
                }

            });
        }
    }

}
