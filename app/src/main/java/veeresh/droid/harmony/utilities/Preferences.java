package veeresh.droid.harmony.utilities;

import net.orange_box.storebox.annotations.method.KeyByString;

/**
 * Created by Veeresh on 2/9/17.
 */

public interface Preferences {

    /* Token */
    @KeyByString("token")
    String getToken();

    @KeyByString("token")
    void setToken(String value);


}
