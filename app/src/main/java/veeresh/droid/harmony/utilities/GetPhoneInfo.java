package veeresh.droid.harmony.utilities;

import android.os.Build;

/**
 * Created by Veeresh8 on 9/1/2016.
 */
public class GetPhoneInfo {

    public static String androidVersion = android.os.Build.VERSION.RELEASE;

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


}
