
package veeresh.droid.harmony.models.StationDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Station extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("stationId")
    @Expose
    private int stationId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("thumb_url")
    @Expose
    private String thumbUrl;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("stream_url")
    @Expose
    private String streamUrl;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("genre_name")
    @Expose
    private String genreName;
    @SerializedName("genre_description")
    @Expose
    private String genreDescription;
    @SerializedName("active")
    @Expose
    private boolean active;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("favourite_count")
    @Expose
    private int favouriteCount;
    @SerializedName("play_count")
    @Expose
    private int playCount;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("inactive_count")
    @Expose
    private int inactiveCount;
    @SerializedName("priority")
    @Expose
    private int priority;
    @SerializedName("is_permanently_off")
    @Expose
    private boolean isPermanentlyOff;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    private boolean isFavorite;

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    private boolean isPlaying;

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public String getGenreDescription() {
        return genreDescription;
    }

    public void setGenreDescription(String genreDescription) {
        this.genreDescription = genreDescription;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getFavouriteCount() {
        return favouriteCount;
    }

    public void setFavouriteCount(int favouriteCount) {
        this.favouriteCount = favouriteCount;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setPlayCount(int playCount) {
        this.playCount = playCount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getInactiveCount() {
        return inactiveCount;
    }

    public void setInactiveCount(int inactiveCount) {
        this.inactiveCount = inactiveCount;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isIsPermanentlyOff() {
        return isPermanentlyOff;
    }

    public void setIsPermanentlyOff(boolean isPermanentlyOff) {
        this.isPermanentlyOff = isPermanentlyOff;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
