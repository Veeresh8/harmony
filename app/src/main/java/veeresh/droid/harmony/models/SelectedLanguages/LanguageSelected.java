package veeresh.droid.harmony.models.SelectedLanguages;

import io.realm.RealmObject;

/**
 * Created by Veeresh on 3/30/17.
 */

public class LanguageSelected extends RealmObject {

    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
