package veeresh.droid.harmony.models.SelectedLanguages;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;

public class HeaderSectionModel extends RealmObject {

    @PrimaryKey
    private String headerTitle;
    private RealmList<Station> languageModelArrayList = new RealmList<>();
    private String type;

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public RealmList<Station> getLanguageModelArrayList() {
        return languageModelArrayList;
    }

    public void setLanguageModelArrayList(RealmList<Station> languageModelArrayList) {
        this.languageModelArrayList = languageModelArrayList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
