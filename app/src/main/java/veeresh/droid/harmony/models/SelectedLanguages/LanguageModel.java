package veeresh.droid.harmony.models.SelectedLanguages;


import io.realm.RealmObject;

public class LanguageModel extends RealmObject {


    private String stationName;
    private String streamURL;
    private String stationImage;

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStreamURL() {
        return streamURL;
    }

    public void setStreamURL(String streamURL) {
        this.streamURL = streamURL;
    }

    public String getStationImage() {
        return stationImage;
    }

    public void setStationImage(String stationImage) {
        this.stationImage = stationImage;
    }


}
