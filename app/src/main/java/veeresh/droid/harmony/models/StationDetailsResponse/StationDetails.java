
package veeresh.droid.harmony.models.StationDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class StationDetails extends RealmObject {

    @SerializedName("station")
    @Expose
    private RealmList<Station> station = null;
    @SerializedName("success")
    @Expose
    private boolean success;

    public RealmList<Station> getStation() {
        return station;
    }

    public void setStation(RealmList<Station> station) {
        this.station = station;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
