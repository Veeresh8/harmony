
package veeresh.droid.harmony.models.AllLanguagesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Language {

    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("language")
    @Expose
    private String language;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}
