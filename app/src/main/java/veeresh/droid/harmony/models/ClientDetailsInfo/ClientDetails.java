package veeresh.droid.harmony.models.ClientDetailsInfo;

/**
 * Created by Veeresh on 3/30/17.
 */

public class ClientDetails {

    private String device_model;
    private String os;
    private String os_version;
    private long device_id;
    private double app_version;
    private String notification_token;
    private boolean is_prod_user;

    public String getDevice_model() {
        return device_model;
    }

    public void setDevice_model(String device_model) {
        this.device_model = device_model;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOs_version() {
        return os_version;
    }

    public void setOs_version(String os_version) {
        this.os_version = os_version;
    }

    public long getDevice_id() {
        return device_id;
    }

    public void setDevice_id(long device_id) {
        this.device_id = device_id;
    }

    public double getApp_version() {
        return app_version;
    }

    public void setApp_version(double app_version) {
        this.app_version = app_version;
    }

    public String getNotification_token() {
        return notification_token;
    }

    public void setNotification_token(String notification_token) {
        this.notification_token = notification_token;
    }

    public boolean is_prod_user() {
        return is_prod_user;
    }

    public void setIs_prod_user(boolean is_prod_user) {
        this.is_prod_user = is_prod_user;
    }
}
