package veeresh.droid.harmony.network;


import net.orange_box.storebox.StoreBox;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import veeresh.droid.harmony.application.HarmonyApplication;
import veeresh.droid.harmony.utilities.AppConstants;
import veeresh.droid.harmony.utilities.Preferences;


public class RetrofitConnection {

    private static final String TAG = RetrofitConnection.class.getSimpleName();
    private static String token;

    public static Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static OkHttpClient provideOkHttpClient() {

        Preferences preferences = StoreBox.create(HarmonyApplication.getInstance(), Preferences.class);

        if (preferences.getToken() != null)
            token = preferences.getToken();
        else
            token = AppConstants.TOKEN;


        return new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request request = original.newBuilder()
                                .header("token", token)
                                .build();
                        return chain.proceed(request);
                    }
                })
                .build();
    }

    private static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    public static APIService getService() {
        return provideRetrofit().create(APIService.class);
    }

}