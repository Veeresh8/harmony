package veeresh.droid.harmony.network;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import veeresh.droid.harmony.models.AllLanguagesResponse.AllLanguagesResponse;
import veeresh.droid.harmony.models.ClientDetailsInfo.ClientDetails;
import veeresh.droid.harmony.models.StationDetailsResponse.StationDetails;

/**
 * Created by Veeresh on 3/11/17.
 */
public interface APIService {

    @POST("api/languages/")
    Call<AllLanguagesResponse> getLanguages(@Body ClientDetails clientDetails);

    @GET("/api/station")
    Call<StationDetails> getStations(@Query("language__in") StringBuilder stations);

}
