package veeresh.droid.harmony.events;

import io.realm.OrderedRealmCollection;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;

/**
 * Created by Veeresh on 3/31/17.
 */

public class MyEvents {


    public static class SelectedLanguageEvent {
        public String selectedLanguage;
        public String type;

        public String getSelectedLanguage() {
            return selectedLanguage;
        }


        public String getType() {
            return type;
        }

        public SelectedLanguageEvent(String selectedLanguage, String type) {
            this.selectedLanguage = selectedLanguage;
            this.type = type;


        }
    }

    public static class FragmentDetachEvent {
        public boolean isDetached;

        public boolean isDetached() {
            return isDetached;
        }

        public FragmentDetachEvent(boolean isDetached) {
            this.isDetached = isDetached;

        }
    }


    public static class ValidateRecyclerViewEvent {
        public boolean mustValidate;

        public boolean isMustValidate() {
            return mustValidate;
        }

        public ValidateRecyclerViewEvent(boolean mustValidate) {
            this.mustValidate = mustValidate;

        }
    }


    public static class CheckFavEvent {
        public boolean mustCheck;


        public CheckFavEvent(boolean mustCheck) {
            this.mustCheck = mustCheck;
        }

        public boolean isMustCheck() {
            return mustCheck;
        }
    }


    public static class IsInProgressEvent {
        public boolean isLoading;

        public IsInProgressEvent(boolean isLoading) {
            this.isLoading = isLoading;
        }

        public boolean isLoading() {
            return isLoading;
        }
    }

    public static class DataLoadedEvent {
        public boolean isDataLoaded;

        public boolean isDataLoaded() {
            return isDataLoaded;
        }

        public DataLoadedEvent(boolean isDataLoaded) {
            this.isDataLoaded = isDataLoaded;

        }
    }

    public static class CarouselEvent {
        public int position;

        public CarouselEvent(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }


    public static class StationPlaybackEvent {
        public Station station;
        public OrderedRealmCollection orderedRealmCollection;
        public int index;

        public int getIndex() {
            return index;
        }

        public Station getStation() {
            return station;
        }

        public OrderedRealmCollection getOrderedRealmCollection() {
            return orderedRealmCollection;
        }

        public StationPlaybackEvent(Station station, OrderedRealmCollection orderedRealmCollection, int index) {
            this.station = station;
            this.index = index;
            this.orderedRealmCollection = orderedRealmCollection;


        }
    }
}
