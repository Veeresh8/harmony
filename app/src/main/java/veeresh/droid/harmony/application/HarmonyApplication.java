package veeresh.droid.harmony.application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import veeresh.droid.harmony.utilities.FontCache;

/**
 * Created by Veeresh on 3/30/17.
 */

public class HarmonyApplication extends Application {

    private static HarmonyApplication instance;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static synchronized HarmonyApplication getInstance() {
        if (instance == null)
            return instance = new HarmonyApplication();
        else
            return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        FontCache.init(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        initRealm();

    }

    private void initRealm() {
        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("Harmony.db")
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
