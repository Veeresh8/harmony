package veeresh.droid.harmony.database;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import io.realm.Case;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import veeresh.droid.harmony.events.MyEvents;
import veeresh.droid.harmony.models.CurrentPlayback.CurrentStation;
import veeresh.droid.harmony.models.SelectedLanguages.HeaderSectionModel;
import veeresh.droid.harmony.models.StationDetailsResponse.Station;

/**
 * Created by Veeresh on 3/30/17.
 */

public class StationsDetailsDAO {


    private Realm mRealm;

    public static final String TAG = StationsDetailsDAO.class.getSimpleName();


    public StationsDetailsDAO(@NonNull Realm realm) {
        mRealm = realm;
    }

    public void saveStations(RealmList<Station> stationRealmList) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Station> rows = realm.where(Station.class).findAll();
                rows.deleteAllFromRealm();
                mRealm.copyToRealm(stationRealmList);

            }
        });
    }

    public void clearDB() {
        mRealm.executeTransaction(realm -> Realm.getDefaultInstance().deleteAll());
    }

    public void saveCurrentlyPlayingQueue(OrderedRealmCollection<Station> stationRealmList) {
        mRealm.executeTransaction(realm -> {

            RealmResults<CurrentStation> result = realm.where(CurrentStation.class).findAll();
            result.deleteAllFromRealm();

            ArrayList<CurrentStation> currentStationArrayList = new ArrayList<>();
            for (int i = 0; i < stationRealmList.size(); i++) {
                CurrentStation currentStation = new CurrentStation();
                currentStation.setStationId(stationRealmList.get(i).getStationId());
                currentStation.setId(stationRealmList.get(i).getId());
                currentStation.setFavorite(stationRealmList.get(i).isFavorite());
                currentStation.setStationName(stationRealmList.get(i).getName());
                currentStation.setStationImage(stationRealmList.get(i).getImageUrl());
                currentStation.setStationCountry(stationRealmList.get(i).getCountryName());
                currentStation.setStreamURL(stationRealmList.get(i).getStreamUrl());
                currentStationArrayList.add(currentStation);
            }
            mRealm.copyToRealmOrUpdate(currentStationArrayList);
        });
    }


    public RealmResults<CurrentStation> getCurrentQueue() {
        return mRealm.where(CurrentStation.class).findAll();
    }

    public boolean hasData() {
        RealmResults<Station> realmResults = mRealm.where(Station.class).findAll();
        return realmResults != null && realmResults.size() > 0;
    }

    public RealmResults<Station> loadStations(String language) {
        RealmQuery<Station> query = mRealm.where(Station.class);
        query.equalTo("language", language);
        RealmResults<Station> realmResults = query.findAll();
        return realmResults.sort("name", Sort.ASCENDING);
    }

    public RealmResults<Station> loadGenre(String genre) {
        RealmQuery<Station> query = mRealm.where(Station.class);
        query.equalTo("genreName", genre);
        RealmResults<Station> realmResults = query.findAll();
        return realmResults.sort("name", Sort.ASCENDING);
    }

    public RealmResults<Station> loadSearch(String query, String language, String type) {

        RealmQuery<Station> realmQuery = mRealm.where(Station.class);
        if (query != null) {
            realmQuery.contains("name", query, Case.INSENSITIVE).findAll();
        }

        switch (type) {
            case "Language": {
                realmQuery.equalTo("language", language);
                break;
            }
            case "Genre": {
                realmQuery.equalTo("genreName", language);
                break;
            }
        }
        return realmQuery.findAll();
    }

    public RealmResults<Station> loadMostPlayed(String language, String type) {
        if (type.equals("Language")) {
            RealmQuery<Station> query = mRealm.where(Station.class);
            query.equalTo("language", language);
            RealmResults<Station> realmResults = query.findAll();
            return realmResults.sort("playCount", Sort.DESCENDING);
        } else {
            RealmQuery<Station> stationRealmQuery = mRealm.where(Station.class);
            stationRealmQuery.equalTo("genreName", language);
            RealmResults<Station> realmResults = stationRealmQuery.findAll();
            return realmResults.sort("playCount", Sort.DESCENDING);
        }


    }


    public RealmResults<Station> loadFavorites(String language, String type) {
        if (type.equals("Language")) {
            RealmQuery<Station> query = mRealm.where(Station.class);
            query.equalTo("language", language);
            RealmResults<Station> realmResults = query.findAll();
            return realmResults.sort("favouriteCount", Sort.DESCENDING);
        } else {
            RealmQuery<Station> stationRealmQuery = mRealm.where(Station.class);
            stationRealmQuery.equalTo("genreName", language);
            RealmResults<Station> realmResults = stationRealmQuery.findAll();
            return realmResults.sort("favouriteCount", Sort.DESCENDING);
        }
    }


    public RealmResults<Station> loadNameAscending(String language, String type) {
        if (type.equals("Language")) {
            RealmQuery<Station> query = mRealm.where(Station.class);
            query.equalTo("language", language);
            RealmResults<Station> realmResults = query.findAll();
            return realmResults.sort("name", Sort.ASCENDING);
        } else {
            RealmQuery<Station> stationRealmQuery = mRealm.where(Station.class);
            stationRealmQuery.equalTo("genreName", language);
            RealmResults<Station> realmResults = stationRealmQuery.findAll();
            return realmResults.sort("name", Sort.ASCENDING);
        }

    }

    public RealmResults<Station> loadNameDescending(String language, String type) {
        if (type.equals("Language")) {
            RealmQuery<Station> query = mRealm.where(Station.class);
            query.equalTo("language", language);
            RealmResults<Station> realmResults = query.findAll();
            return realmResults.sort("name", Sort.DESCENDING);
        } else {
            RealmQuery<Station> stationRealmQuery = mRealm.where(Station.class);
            stationRealmQuery.equalTo("genreName", language);
            RealmResults<Station> realmResults = stationRealmQuery.findAll();
            return realmResults.sort("name", Sort.DESCENDING);
        }

    }


    public void saveToFavorites(Station station) {
        mRealm.executeTransaction(realm -> {
            station.setFavorite(true);
            mRealm.copyToRealmOrUpdate(station);
        });
    }

    public void removeFromFavorites(Station station) {
        mRealm.executeTransaction(realm -> {
            station.setFavorite(false);
            mRealm.copyToRealmOrUpdate(station);
        });
    }

    public void setIsPlaying(int stationID) {
        mRealm.executeTransaction(realm -> {
            RealmResults<Station> realmResults = mRealm.where(Station.class).findAll();
            for (int i = 0; i < realmResults.size(); i++) {
                if (realmResults.get(i).getStationId() == stationID) {
                    realmResults.get(i).setPlaying(true);
                } else {
                    realmResults.get(i).setPlaying(false);
                }
            }
            mRealm.copyToRealmOrUpdate(realmResults);
        });
    }

    public void saveSectionData(ArrayList<HeaderSectionModel> sectionModelArrayList) {
        mRealm.executeTransaction(realm -> mRealm.copyToRealmOrUpdate(sectionModelArrayList));
    }

    public void saveSectionItem(MyEvents.StationPlaybackEvent playbackEvent) {
        mRealm.executeTransaction(realm -> {
            HeaderSectionModel realmResults = RealmManager.stationsDetailsDAO().loadHeaderData();
            if (realmResults.getLanguageModelArrayList().contains(playbackEvent.getStation())) {
                realmResults.getLanguageModelArrayList().remove(playbackEvent.getStation());
            }
            realmResults.getLanguageModelArrayList().add(0, playbackEvent.getStation());
            mRealm.copyToRealmOrUpdate(realmResults);
        });
    }


    public void saveSectionItemById(int stationID) {
        mRealm.executeTransaction(realm -> {

            Station station = mRealm.where(Station.class).equalTo("stationId", stationID).findFirst();

            HeaderSectionModel realmResults = RealmManager.stationsDetailsDAO().loadHeaderData();
            if (realmResults.getLanguageModelArrayList().contains(station)) {
                realmResults.getLanguageModelArrayList().remove(station);
            }
            realmResults.getLanguageModelArrayList().add(0, station);
            mRealm.copyToRealmOrUpdate(realmResults);
        });
    }


    public boolean checkIfRecentExists(String title) {
        RealmResults<HeaderSectionModel> realmResults = mRealm.where(HeaderSectionModel.class).equalTo("headerTitle", title).findAll();
        return realmResults != null && realmResults.size() > 0;
    }

    public RealmResults<HeaderSectionModel> loadSectionData() {
        return mRealm.where(HeaderSectionModel.class).findAll();
    }

    public HeaderSectionModel loadHeaderData() {
        return mRealm.where(HeaderSectionModel.class).equalTo("type", "Header").findFirst();
    }

    public RealmResults<Station> loadFavStations() {
        return mRealm.where(Station.class).equalTo("isFavorite", true).findAll();
    }
}
