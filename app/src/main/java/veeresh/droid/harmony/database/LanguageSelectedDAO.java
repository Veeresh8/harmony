package veeresh.droid.harmony.database;

import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import veeresh.droid.harmony.models.SelectedLanguages.LanguageSelected;

/**
 * Created by Veeresh on 3/30/17.
 */

public class LanguageSelectedDAO {


    private Realm mRealm;

    public LanguageSelectedDAO(@NonNull Realm realm) {
        mRealm = realm;
    }

    public void saveLanguageSelected(RealmList<LanguageSelected> languageSelectedRealmList) {
        mRealm.executeTransaction(realm -> mRealm.copyToRealm(languageSelectedRealmList));
    }

    public RealmResults<LanguageSelected> loadLanguagesSelected() {
        return mRealm.where(LanguageSelected.class).findAll();
    }




}
